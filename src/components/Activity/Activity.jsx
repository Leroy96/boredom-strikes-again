import React from "react";

const style = {
    column: {
        display: 'flex',
        flexDirection: 'column'
    }
}

const Activity = ({ results }) => {
    return (
        <>
            <div className="row">
                <div className="col-1" style={style.column}>
                    <span>Activity:</span>
                    <span>Type:</span>
                    <span>Price:</span>
                    <span>Participants:</span>
                </div>
                <div className="col-5" style={style.column}>
                    <span>{results.activity}</span>
                    <span>{results.type}</span>
                    <span>{results.price}</span>
                    <span>{results.participants}</span>
                </div>
            </div>
        </>
    )
}

export default Activity